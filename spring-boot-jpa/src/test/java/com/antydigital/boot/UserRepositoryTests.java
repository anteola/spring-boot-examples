package com.antydigital.boot;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.antydigital.boot.jpa.repository.UserRepository;

@DataJpaTest
@RunWith(SpringRunner.class)
@SpringBootApplication(
		exclude = { DataSourceAutoConfiguration.class,
					HibernateJpaAutoConfiguration.class,
					DataSourceTransactionManagerAutoConfiguration.class
		}
)
public class UserRepositoryTests {
	@Autowired
	private UserRepository userRepository;

	@Test
	public void check_todo_count() {
		assertEquals(3, userRepository.count());
	}
}
