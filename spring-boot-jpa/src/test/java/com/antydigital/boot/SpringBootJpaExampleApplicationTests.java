package com.antydigital.boot;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.antydigital.boot.jpa.entity.User;
import com.antydigital.boot.jpa.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootJpaExampleApplicationTests {

	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void findAllUsers() {
		List<User> users = userRepository.findAll();
		System.err.println(users);
		assertNotNull(users);
	}
}
