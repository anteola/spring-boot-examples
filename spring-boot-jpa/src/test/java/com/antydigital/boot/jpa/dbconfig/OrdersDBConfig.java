package com.antydigital.boot.jpa.dbconfig;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Specify at the class level where the jpa repositories are located
 * @author Chris
 *
 */
@Configuration
@EnableJpaRepositories(
		basePackages = "com.antydigital.boot.jpa.repository", 
		entityManagerFactoryRef = "ordersEntityManagerFactory", 
		transactionManagerRef = "ordersTransactionManager"
)
public class OrdersDBConfig {

	@Autowired
	private Environment env;
	   
	/**
	 * The configuration properties prefix links to the application.properties
	 * prefix
	 * 
	 * @return
	 */
	@Bean
	@ConfigurationProperties(prefix = "spring.h2")
	public DataSourceProperties ordersDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	public DataSource ordersDataSource() {
		DataSourceProperties primaryDataSourceProperties = ordersDataSourceProperties();
		return DataSourceBuilder.create()
				.driverClassName(primaryDataSourceProperties.getDriverClassName())
				.url(primaryDataSourceProperties.getUrl())
				.username(primaryDataSourceProperties.getUsername())
				.password(primaryDataSourceProperties.getDataPassword())
				.build();
	}

	/**
	 * Specify where to look for Entity Beans
	 * @return
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean ordersEntityManagerFactory() {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(ordersDataSource());
		factory.setPackagesToScan("com.antydigital.boot.jpa.entity");
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		
		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.hbm2ddl.auto",  env.getProperty("hibernate.hbm2ddl.auto"));
		jpaProperties.put("hibernate.show-sql", env.getProperty("hibernate.show-sql"));
		jpaProperties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
		factory.setJpaProperties(jpaProperties);
		
		return factory;
	}

	@Bean
	public PlatformTransactionManager ordersTransactionManager() {
		EntityManagerFactory factory = ordersEntityManagerFactory().getObject();
		return new JpaTransactionManager(factory);
	}
	
	@Bean
	public DataSourceInitializer ordersDataSourceInitializer()
	{
		DataSourceInitializer dsInitializer = new DataSourceInitializer();
		dsInitializer.setDataSource(ordersDataSource());
		ResourceDatabasePopulator dbPopulator = new ResourceDatabasePopulator();
		dbPopulator.addScript(new ClassPathResource("test-h2.sql"));
		dsInitializer.setDatabasePopulator(dbPopulator);
		dsInitializer.setEnabled(env.getProperty("spring.h2.initialize", Boolean.class, false));
		System.err.println("In Orders DSInitializer");
		return dsInitializer;
	}
}
