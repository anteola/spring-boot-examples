package com.antydigital.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/*
 * Database auto configurations are turned off by the exclude
 * Enable TransactionManagement explicity by using the @EnableTransactionManagement annotation
 */
@SpringBootApplication(
		exclude = { DataSourceAutoConfiguration.class,
					HibernateJpaAutoConfiguration.class,
					DataSourceTransactionManagerAutoConfiguration.class
		}
)
@EnableTransactionManagement
@ActiveProfiles("test")
public class SpringBootJpaExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJpaExampleApplication.class, args);
	}
}
