package com.antydigital.boot.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.antydigital.boot.jpa.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
